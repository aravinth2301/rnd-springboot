package au.xbits.util;

public enum Error {

    USER_AUTH_NOT_FOUND("User requires at least one role"),
    ROLE_REQUIRE_FUNCTION("Role requires at least one function");
    private final String description;

    Error(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
