package au.xbits.util;

import au.xbits.domain.Function;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SERVICE_URL {


    public static final String ROLE_ADD = "/role";
    public static final String ROLE_FETCH_BY_NAME = "/role/{name}";
    public static final String FUNCTION_FETCH = "/functions";

    public static final String USER_ADD = "/user";
    public static final String USER_FETCH = "/users";
    public static final String USER_FETCH_BY_NAME = "/user/{name}";
    public static final String USER_FETCH_BY_ID = "/user-by-id/{userId}";


    public static final String CHANGE_PASSWORD = "/change-password";




    public static final String TEST_CLIENT_DB_CONF = "/test_client_db_conf";

    private static List<Function> functions = new ArrayList<Function>();

    private static List<Function> activatedFunctions = new ArrayList<Function>();

    static {
        Field[] fields = SERVICE_URL.class.getDeclaredFields();
        for (int i = 0; i < fields.length ; i++) {
            Field field = fields[i];
            functions.add(new Function(field.getName()));
        }
    }

    static {
        Field[] fields = SERVICE_URL.class.getDeclaredFields();
        for (int i = 0; i < fields.length ; i++) {
            Field field = fields[i];
            activatedFunctions.add(new Function(field.getName(),true));
        }
    }


    public static List<Function> getActivatedFunctions() {
        return activatedFunctions;
    }

    public static List<Function> getFunctions() {
        return functions;
    }
}
