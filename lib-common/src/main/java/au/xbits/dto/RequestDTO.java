package au.xbits.dto;

import au.xbits.domain.AppUser;
import au.xbits.domain.Function;
import au.xbits.util.JSONUtil;

import java.io.Serializable;

public class RequestDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -9152981833787522252L;

	private AppUser appUser;

    private Function function;

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

	@Override
	public String toString() {
		return JSONUtil.toString(this);
	}
    
    
}
