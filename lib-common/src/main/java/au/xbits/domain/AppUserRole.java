package au.xbits.domain;

import javax.persistence.*;

@Entity
public class AppUserRole extends _Domain{


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_role_generator")
    @SequenceGenerator(name="user_role_generator", sequenceName = "user_role_seq")
    private Long id;

    @ManyToOne
    private AppUser appUser;

    @ManyToOne
    private Role role;

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public AppUserRole() {
    }

    public AppUserRole(AppUser appUser, Role role) {
        this.appUser = appUser;
        this.role = role;
    }
}
