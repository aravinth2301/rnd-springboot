package au.xbits.domain;

import au.xbits.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class _Domain {
    private Date createdAt = new Date();

    @Column(insertable = false,updatable = false)
    private AppUser createdBy;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public AppUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AppUser createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return JSONUtil.toString(this);
    }
}
