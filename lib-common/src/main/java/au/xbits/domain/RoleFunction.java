package au.xbits.domain;

import javax.persistence.*;

@Entity
public class RoleFunction extends _Domain {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_function_generator")
    @SequenceGenerator(name="role_function_generator", sequenceName = "role_function_seq")
    private Long id;

    @ManyToOne
    private Role role;

    @ManyToOne
    private Function function;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public RoleFunction() {
    }

    public RoleFunction(Role role, Function function) {
        this.role = role;
        this.function = function;
    }
}
