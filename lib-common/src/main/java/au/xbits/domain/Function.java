package au.xbits.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Function extends _Domain implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "function_generator")
    @SequenceGenerator(name="function_generator", sequenceName = "function_seq")
    private Long id;

    @Column(unique = true)
    private String code;

    private Boolean enabled = false;

    private String description;

    @Transient
    private Boolean delete = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Function() {
    }

    public Function(String code) {
        this.code = code;
    }

    public Function(String code, Boolean enabled) {
        this.code = code;
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Function)) return false;
        Function function = (Function) o;
        return Objects.equals(getCode(), function.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }
}
