package au.xbits.dao;

import au.xbits.domain.AppUserRole;

import java.util.List;

public interface AppUserRoleDao {
    List<AppUserRole> findUserRole(Long userId, Long roleId);
}
