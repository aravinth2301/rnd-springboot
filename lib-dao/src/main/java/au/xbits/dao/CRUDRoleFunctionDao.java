package au.xbits.dao;

import au.xbits.domain.Function;
import au.xbits.domain.RoleFunction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CRUDRoleFunctionDao extends CrudRepository<RoleFunction,Long> {
    @Query("select rf from RoleFunction rf")
    public List<Function> fetchAll();
}
