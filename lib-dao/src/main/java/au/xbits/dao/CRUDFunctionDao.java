package au.xbits.dao;

import au.xbits.domain.Function;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CRUDFunctionDao extends CrudRepository<Function,Long> {

    @Query("select p from Function p")
    public List<Function> fetchAll();
}
