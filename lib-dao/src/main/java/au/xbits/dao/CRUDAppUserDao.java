package au.xbits.dao;

import au.xbits.domain.AppUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CRUDAppUserDao extends CrudRepository<AppUser,Long> {

    @Query("select p from AppUser p where p.username =:username")
    AppUser findAppUser(String username);


    @Query("select p from AppUser p")
    List<AppUser> findAllUsers();

}
