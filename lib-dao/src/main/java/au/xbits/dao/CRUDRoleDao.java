package au.xbits.dao;

import au.xbits.domain.AppUser;
import au.xbits.domain.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CRUDRoleDao extends CrudRepository<Role,Long> {

    @Query("select r from Role r where r.name =:name")
    Role findRole(String name);


}
