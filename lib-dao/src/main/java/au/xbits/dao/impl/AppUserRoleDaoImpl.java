package au.xbits.dao.impl;

import au.xbits.dao.AppUserRoleDao;
import au.xbits.domain.AppUserRole;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AppUserRoleDaoImpl implements AppUserRoleDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<AppUserRole> findUserRole(Long userId, Long roleId) {
        String q = "select p from AppUserRole p " +
                "where " +
                "p.appUser.id=:userId and " +
                "p.role.id=:roleId";
        Query query = entityManager.createQuery(q);
        query.setParameter("userId", userId);
        query.setParameter("roleId", roleId);
        return query.getResultList();
    }
}
