package au.xbits.dao;

import au.xbits.domain.AppUserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CRUDAppUserRoleDao extends CrudRepository<AppUserRole,Long> , AppUserRoleDao{

    @Query("select ur from AppUserRole ur where ur.appUser.id =:userId")
    List<AppUserRole> findAppUserRoleByUser(Long userId);
}
