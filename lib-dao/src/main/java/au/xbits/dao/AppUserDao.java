package au.xbits.dao;

import au.xbits.domain.AppUser;

import java.util.List;

public interface AppUserDao {

    AppUser findAppUser(String username);

    List<AppUser> findAllUsers();
}
