package au.xbits.controller;

import au.xbits.domain.AppUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class VueController {

    private static final String vuePath = "../static/vue/index";
    @GetMapping("/vapp/**")
    public String vueIndex(Model model, HttpServletRequest request, HttpServletResponse response,Authentication authentication) throws IOException {
        AppUser appUser = (AppUser) authentication.getPrincipal();
        if(!"/vapp/change-password".equals(request.getServletPath())&& appUser.getLastLoginAt()==null)
        	response.sendRedirect("/vapp/change-password");
        return vuePath;
    }

    @GetMapping("/vapp/api/current-user")
    public @ResponseBody ResponseEntity<AppUser>  vueCurrentUser(Authentication authentication){
        AppUser appUser = (AppUser) authentication.getPrincipal();
        return new ResponseEntity<AppUser>(appUser,HttpStatus.OK);
    }

    @GetMapping("/app-login")
    public String login(Model model) {
        return vuePath;
    }

    @GetMapping("/auth")
    public String auth(Model model) {
        return "auth";
    }

    @GetMapping("/test")
    public String test(Model model) {
        return "test";
    }
}
