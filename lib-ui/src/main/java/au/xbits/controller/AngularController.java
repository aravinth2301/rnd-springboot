package au.xbits.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class AngularController {
    @RequestMapping(value = "/ng/home",method = RequestMethod.GET)
    public String angularIndex(Model model, HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        return "ang-index";
    }
}
