'use strict'

var demoApp = angular.module('demo', [ 'ui.bootstrap', 'demo.controllers',
		'demo.services' ]);
demoApp.constant("CONSTANTS", {
	getUserByIdUrl : "/api/v1/user-by-id/",
	getAllUsers : "/user/getAllUsers",
	saveUser : "/user/saveUser"
});