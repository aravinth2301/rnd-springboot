#!/usr/bin/env bash

dest=lib-ui/src/main/resources/static
src=vue-app/dist
vindex=lib-ui/src/main/resources/templates/vue-index.html

cd ../
cd vue-app
npm run build
cd ../


rm -rf ${dest}/css
rm -rf ${dest}/js
rm -rf ${dest}/img
rm -rf ${dest}/favicon.ico
rm -rf ${vindex}
echo "\nDeleted successfully from ${dest} \n"

cp -r ${src}/css ${dest}
cp -r ${src}/js ${dest}
cp -r ${src}/img ${dest}
cp -r ${src}/favicon.ico ${dest}
cp -r ${src}/index.html ${vindex}

echo "Copied successfully from ${src} \n"
