package au.xbits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceMain {
    public static void main(String[] args) {
        System.setProperty("spring.config.name","application-dao,application");
        SpringApplication.run(ServiceMain.class,args);
    }
}
