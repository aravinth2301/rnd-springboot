#!/usr/bin/env bash
/Applications/Google\ Chrome.app/contents/MacOS/Google\ Chrome \
--args \
--disable-web-security \
--user-data-dir="$TMPDIR/xbits/openlink/chrome"