FROM ubuntu:latest
RUN apt-get -y update --fix-missing
RUN apt-get -y install openjdk-8-jdk
RUN apt-get -y update
RUN apt-get -y install git
RUN apt-get -y install maven
RUN apt-get -y install npm
RUN npm i -g @vue/cli
RUN npm i -g @angular/cli