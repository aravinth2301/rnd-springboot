package au.xbits.service.impl;

import au.xbits.dao.*;
import au.xbits.domain.*;
import au.xbits.service.AppUserService;

import au.xbits.util.Error;
import au.xbits.util.SERVICE_URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class AppUserServiceImpl implements AppUserService {

    private static final Logger log = LoggerFactory.getLogger(AppUserServiceImpl.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CRUDAppUserDao crudAppUserDao;

    @Autowired
    private CRUDRoleDao crudRoleDao;

    @Autowired
    private CRUDAppUserRoleDao crudAppUserRoleDao;

    @Autowired
    private CRUDFunctionDao crudFunctionDao;

    @Autowired
    private CRUDRoleFunctionDao crudRoleFunctionDao;

    @Autowired
    private AppUserDao appUserDao;

    @PostConstruct
    private void init(){
        addMetaUser();
        addMetaFunctions();
    }
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return crudAppUserDao.findAppUser(s);
    }

    @Override
    //@PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<AppUser> fetchAppUsers(AppUser appUser) {
        List<AppUser> appUsers = null;
        if(appUser==null || appUser.getUsername()==null)
            appUsers =  crudAppUserDao.findAllUsers();
        else
            appUsers  = new ArrayList<AppUser>();
        return appUsers;
    }

    @Override
    public AppUser fetchAppUser(String appUser) {
        return crudAppUserDao.findAppUser(appUser);
    }

    @Override
    public AppUser fetchAppUsers(Long userId) {
        return crudAppUserDao.findById(userId).get();
    }

    @Override
    public List<Function> fetchFunctions(){
        return crudFunctionDao.fetchAll();
    }

    @Override
    public Role fetchRole(String name){
        return crudRoleDao.findRole(name);
    }


    @Override
    @Transactional
    public AppUser saveUser(AppUser appUser){
        if(log.isDebugEnabled()){
            log.debug(appUser.toString());
        }
        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        AppUser savedUser = crudAppUserDao.save(appUser);
        if(appUser.getAuthorities()!=null){
            appUser.getAuthorities().forEach(role->{
                if(role.getDelete()!=null && role.getDelete() ){
                    AppUserRole appUserRole = new AppUserRole(appUser,role);
                    crudAppUserRoleDao.delete(appUserRole);
                }else{
                    List<AppUserRole> userRoles = crudAppUserRoleDao.findUserRole(appUser.getId(), role.getId());
                    if(!(userRoles != null && !userRoles.isEmpty())){
                        AppUserRole appUserRole = new AppUserRole(appUser,role);
                        appUserRole.setCreatedAt(new Date());
                        crudAppUserRoleDao.save(appUserRole);
                    }
                }

            });
        }else{
            throw new RuntimeException(Error.USER_AUTH_NOT_FOUND.getDescription());
        }

        assignRole(savedUser, crudAppUserRoleDao.findAppUserRoleByUser(savedUser.getId()));

        if(log.isDebugEnabled()){
            log.debug("User saved successfully "+savedUser.getUsername());
        }
        return savedUser;
    }

    @Override
    public AppUser updateLastLogin(AppUser appUser) {
        AppUser user = crudAppUserDao.findById(appUser.getId()).get();
        if(appUser.getNewPassword() != null)
            user.setPassword(passwordEncoder.encode(appUser.getNewPassword()));
        user.setLastLoginAt(new Date());
        return crudAppUserDao.save(user);
    }

    @Override
    @Transactional
    public Role saveRole(Role role){
        role=crudRoleDao.save(role);
        if(role.getFunctions()!=null){
            for (Function function : role.getFunctions()) {
                RoleFunction roleFunction = new RoleFunction(role,function);
                if(function.getDelete()){
                    crudRoleFunctionDao.delete(roleFunction);
                }else{
                    crudRoleFunctionDao.save(roleFunction);
                }
            }
        }else{
            // throw new RuntimeException(Error.ROLE_REQUIRE_FUNCTION.getDescription());
        }
        return role;
    }

    @Override
    public AppUser testClientDBConfig(){
        return appUserDao.findAppUser("");
    }

    private void addMetaUser(){
        AppUser appUser = crudAppUserDao.findAppUser("admin");
        Role role = crudRoleDao.findRole("R_ADMIN");

        if(role == null){
            role = new Role();
            role.setName("R_ADMIN");
            role.setCreatedAt(new Date());
            role = crudRoleDao.save(role);
        }

        if(appUser==null){
            appUser = new AppUser();
            appUser.setUsername("admin");
            appUser.setPassword(passwordEncoder.encode("admin"));
            appUser.setCreatedAt(new Date());
            appUser= crudAppUserDao.save(appUser);
            AppUserRole appUserRole = new AppUserRole(appUser,role);
            appUserRole.setCreatedAt(new Date());
            crudAppUserRoleDao.save(appUserRole);
        }

        if(log.isDebugEnabled()){
            log.debug("User Id"+appUser.getId()+"");
        }
    }


    private void addMetaFunctions(){
        List<Function> functions = crudFunctionDao.fetchAll();
        if(functions==null || functions.isEmpty())
            SERVICE_URL.getFunctions().forEach(function -> {
                crudFunctionDao.save(function);
            });
        //functions = crudFunctionDao.fetchAll();
    }

    private void assignRole(AppUser user,List<AppUserRole> userRoles){
        List<Role> roles = new ArrayList<Role>();
        if(userRoles!=null && !userRoles.isEmpty()){
            userRoles.forEach( userRole ->{
                roles.add(userRole.getRole());
            });
        }
        user.setAuthorities(roles);
    }
}