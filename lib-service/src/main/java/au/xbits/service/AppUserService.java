package au.xbits.service;

import au.xbits.domain.AppUser;
import au.xbits.domain.Function;
import au.xbits.domain.Role;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface AppUserService extends UserDetailsService {

    List<AppUser> fetchAppUsers(AppUser appUser);

    AppUser fetchAppUser(String appUser);

    AppUser fetchAppUsers(Long userId);

    List<Function> fetchFunctions();

    Role fetchRole(String name);

    AppUser saveUser(AppUser appUser);

    AppUser updateLastLogin(AppUser appUser);

    Role saveRole(Role role);

    AppUser testClientDBConfig();
}
