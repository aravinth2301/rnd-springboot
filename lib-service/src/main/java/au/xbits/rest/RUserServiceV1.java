package au.xbits.rest;

import au.xbits.domain.AppUser;
import au.xbits.domain.Function;
import au.xbits.domain.Role;
import au.xbits.dto.RequestDTO;
import au.xbits.service.AppUserService;
import au.xbits.util.SERVICE_URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class RUserServiceV1 extends _RService {

	private static final Logger log = LoggerFactory.getLogger(RUserServiceV1.class);

	@Autowired
	private AppUserService appUserService;


	@RequestMapping(value = SERVICE_URL.ROLE_ADD, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Role> saveRole(@RequestBody Role role) {
		return success(appUserService.saveRole(role));
	}

	@RequestMapping(value = SERVICE_URL.ROLE_FETCH_BY_NAME, method = RequestMethod.GET)
	public ResponseEntity<Role> fetchRole(@PathVariable("name") String name) {
		return success(appUserService.fetchRole(name));
	}

	@RequestMapping(value = SERVICE_URL.USER_FETCH_BY_NAME, method = RequestMethod.GET)
	public ResponseEntity<AppUser> fetchUser(@PathVariable("name") String name) {
		return success(appUserService.fetchAppUser(name));
	}

	@RequestMapping(value = SERVICE_URL.USER_FETCH_BY_ID, method = RequestMethod.GET)
	public ResponseEntity<AppUser> fetchUser(@PathVariable("userId") Long userId) {
		return success(appUserService.fetchAppUsers(userId));
	}

	@RequestMapping(value = SERVICE_URL.USER_ADD, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<AppUser> saveUser(@RequestBody RequestDTO requestDTO) {
		return success(appUserService.saveUser(requestDTO.getAppUser()));
	}

	@RequestMapping(value = SERVICE_URL.USER_FETCH, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<AppUser>> users(@RequestBody AppUser appUser) {
		return success(appUserService.fetchAppUsers(appUser));
	}

	@RequestMapping(value = SERVICE_URL.FUNCTION_FETCH, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<Function>> fetchFunction(@RequestBody RequestDTO requestDTO) {
		return success(appUserService.fetchFunctions());
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> test() {
		return success("Hello");
	}

	@RequestMapping(value = SERVICE_URL.CHANGE_PASSWORD, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<AppUser> changePassword(@RequestBody RequestDTO requestDTO) {
		if (log.isDebugEnabled()) {
			log.debug(requestDTO.toString());
		}
		return success(appUserService.updateLastLogin(requestDTO.getAppUser()));
	}

	@RequestMapping(value = SERVICE_URL.TEST_CLIENT_DB_CONF, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<AppUser> testUserDBConf(){
		return success(appUserService.testClientDBConfig());
	}
}
