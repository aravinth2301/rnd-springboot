package au.xbits.rest;

import au.xbits.domain.AppUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class _RService {
    public <T> ResponseEntity<T> success(T data) {
        if(data instanceof AppUser){
            ((AppUser)data).setPassword(null);
        }
        return respondWith(data, HttpStatus.OK);
    }
    public <T> ResponseEntity<T> respondWith(T data, HttpStatus statusCode) {
        return new ResponseEntity<T>(data, statusCode);
    }
}
