package au.xbits.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class AppWebSecurityConfig extends WebSecurityConfigurerAdapter
{
    private static final Logger log = LoggerFactory.getLogger(AppWebSecurityConfig.class);
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        if(log.isDebugEnabled()){
            log.debug("App Security Init ");
        }
        http
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/app-login*").permitAll()
//                .antMatchers("/h2/**").permitAll()
                .antMatchers("/vapp/**").authenticated()
                .antMatchers("/ng/**").authenticated()
                //.antMatchers("/api/**").authenticated()
                .and()
            .formLogin()
                .loginPage("/app-login")
                .loginProcessingUrl("/app-login")
                .defaultSuccessUrl("/vapp/home")
                .failureUrl("/app-login?error=true")
                .successHandler(getAppSuccessHandler())
                .permitAll()
                .and()
            .logout()
//                .logoutUrl("/perform_logout")
                .deleteCookies("JSESSIONID")
                .permitAll();
    }

    private AuthenticationSuccessHandler getAppSuccessHandler() {
        return new AppLoginSuccessHandler();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}