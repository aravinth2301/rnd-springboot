package au.xbits.security;

import au.xbits.domain.AppUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AppLoginSuccessHandler implements AuthenticationSuccessHandler {
    private static final Logger log = LoggerFactory.getLogger(AppLoginSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        AppUser appUser = (AppUser) authentication.getPrincipal();
        log.info("logged "+appUser.getUsername());
        response.setHeader("test","testValue");
        request.setAttribute("user","Test");
        if (appUser.getLastLoginAt() == null) {
            response.sendRedirect("/vapp/change-password");
        } else {
            response.sendRedirect("/vapp/dashboard");
        }

    }

}
