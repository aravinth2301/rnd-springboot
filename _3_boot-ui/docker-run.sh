#!/usr/bin/env bash

$(pwd)/cp-vue.sh
cd ../
mvn clean install -X
cd _3_boot-ui
docker run \
--rm \
-p 8000:8000 \
-v $(pwd)/target:/tmp \
xbits/rnd-boot-ui