module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        "eslint:recommended",
        "plugin:vue/essential",
        "plugin:vue/strongly-recommended",
        // "plugin:prettier/recommended",
        // "@vue/prettier"
    ],
    rules: {
        "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
        // "disable-next-line": "off",
        "vue/max-attributes-per-line": 10,
        "vue/html-indent": 2,
        "no-unused-vars": ["error", {
            "ignoreRestSiblings": true
        }]
        // "max-attributes-per-line": [10, {
        // "singleline": 10,
        // "multiline": {
        // "max": 1,
        // }
        // }]
        // "function-paren-newline": ["error", "multiline"],
    },
    parserOptions: {
        parser: "babel-eslint",
    },
};