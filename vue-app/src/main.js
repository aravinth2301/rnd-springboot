import Vue from 'vue';
import Vuex from 'vuex'
import App from './App.vue';
import router from "./app-route";
import VueRouter from 'vue-router';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPowerOff } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faPowerOff);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.config.productionTip = false;

new Vue({
  router,
  data:function(){
    return{
      username: "Hello"
    }
  },
  render: h => h(App),
}).$mount('#app');