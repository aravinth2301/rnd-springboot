import Vue from "vue";
import VueRouter from "vue-router";


Vue.use(VueRouter);
let pgPrefix = '/vapp';
let routes = [
    {
        path: '/app-login',
        component: () => import("@/Login")
    },{
        path: pgPrefix+'/home',
        component: () => import("@/components/Home")
    },{
        path: pgPrefix+'/change-password',
        component: () => import("@/components/ChangePassword")
    },{
        path: pgPrefix+'/view-users',
        component: () => import("@/components/user/ViewUsers")
    },{
        path: pgPrefix+'/dashboard',
        component: () => import("@/components/Dashboard")
    }
];
export default new VueRouter({
    mode: "history",
    routes: routes
});
