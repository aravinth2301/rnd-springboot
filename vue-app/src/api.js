import axios from 'axios';

export const vappApi = axios.create({
    baseURL: "http://localhost:8000/vapp/api",
    headers: {
        "Content-type": "application/json",
    }
});

export const servApi = axios.create({
    baseURL: "http://localhost:8000/api",
    headers: {
        "Content-type": "application/json",
    }
});

export default axios;