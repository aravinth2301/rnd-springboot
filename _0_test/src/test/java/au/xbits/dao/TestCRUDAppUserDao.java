package au.xbits.dao;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import au.xbits.domain.AppUser;
import au.xbits.domain.AppUserRole;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestCRUDAppUserDao {
    private static final Logger log = LoggerFactory.getLogger(TestCRUDAppUserDao.class);

    @Autowired
    private CRUDAppUserDao CRUDAppUserDao;

    @Autowired
    private CRUDAppUserRoleDao crudAppUserRoleDao;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void addUser() {

        AppUser user = new AppUser();
        user.setUsername("TESTUSER");
        user.setPassword(passwordEncoder.encode("admin"));
        user.setCreatedAt(new Date());
        CRUDAppUserDao.save(user);

    }

    @Test
    public void fetchAll() {
        addUser();
        Iterable<AppUser> users = CRUDAppUserDao.findAll();
        log.info(" Test Print ");
        users.forEach(user -> {
            log.info(user.getUsername());
        });
        assertThat(users).isNotNull();
    }

    @Test
    public void fetchByUsername() {
        addUser();
        AppUser user = CRUDAppUserDao.findAppUser("TESTUSER");
        assertThat(user).isNotNull();
    }

    @Test
    public void fetchUserRole(){
        List<AppUserRole> appRole = crudAppUserRoleDao.findUserRole(1l,1l);
        assertThat(appRole).isNotNull();
    }

}
