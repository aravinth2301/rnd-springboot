package au.xbits.util;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.xbits.domain.AppUser;
import au.xbits.domain.Function;
import au.xbits.domain.Role;
import au.xbits.dto.RequestDTO;
import au.xbits.util.SERVICE_URL;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class _IntegrationTestBase {

    protected static final String USERNAME="T_USER";
    protected static final String ROLENAME="T_ROLE";

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected TestRestTemplate testRestTemplate;

    @Autowired
    private WebApplicationContext context;

    protected MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    protected ResultActions newRole(String name, List<Function> functions) throws Exception{
        Role role = new Role();
        role.setName(name);
        if(functions==null){
            functions = SERVICE_URL.getFunctions();
        }
        role.setFunctions(functions);

        return mvc.perform(post("/api/v1/role")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(role)));
    }

    protected ResultActions newUser(String username,Role role) throws Exception{
        AppUser user = new AppUser();
        if(username==null)
            user.setUsername(randomString(7).toUpperCase());
        else
            user.setUsername(username);
        user.setPassword("TEST");
        List<Role> roles = new ArrayList<Role>();
        roles.add(role);
        user.setAuthorities(roles);
        RequestDTO res = new RequestDTO();
        res.setAppUser(user);
        return mvc.perform(post("/api/v1/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(res)));
    }

    public ResultActions fetchUsers(AppUser appUser) throws Exception {
        String content  = appUser !=null ? objectMapper.writeValueAsString(appUser) :"{}";
        ResultActions response = mvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));
        return response;
    }

    @SuppressWarnings("unchecked")
	protected List<AppUser> getUsers() throws Exception{
        ResultActions response =  fetchUsers(null);
        return objectMapper.readValue(response.andReturn().getResponse().getContentAsString(),List.class);
    }


    public ResultActions fetchRole(String name) throws Exception {
        ResultActions response = mvc.perform(get("/api/v1/role/{name}",new Object[]{name}));
        return response;
    }


	protected AppUser getUser(String username) throws Exception {
		ResultActions response;
		if (username == null) {
			response = newUser(randomString(6).toUpperCase(), getRole(null));
		} else {
			response = mvc.perform(get("/api/v1/user/{username}", new Object[] { username }));
		}
		AppUser savedUser = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(),
				AppUser.class);
		return savedUser;
	}

    protected Role getRole(String name,List<Function> functions) throws Exception{
        String strRole = fetchRole(name).andReturn().getResponse().getContentAsString();
        Role savedRole = null;
        if(strRole == null)
            savedRole = objectMapper.readValue(strRole,Role.class);
        else
            savedRole = objectMapper.readValue(newRole(name==null?randomString(5):name,functions).andReturn().getResponse().getContentAsString(),Role.class);
        return savedRole;
    }

    protected Role getRole(String name) throws Exception{
    	
    	
    	
        return getRole(name,null);
    }

    protected String randomString(int count){
    	String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < count) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
}
