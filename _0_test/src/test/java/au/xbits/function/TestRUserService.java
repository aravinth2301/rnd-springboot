package au.xbits.function;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;

import au.xbits.domain.AppUser;
import au.xbits.dto.RequestDTO;
import au.xbits.util.SERVICE_URL;
import au.xbits.util._IntegrationTestBase;

public class TestRUserService extends _IntegrationTestBase{

    @Test
    @WithMockUser("admin")
    public void addRole() throws Exception {
        ResultActions response = newRole(randomString(5).toUpperCase(),null);
        response.andExpect(status().isOk());
    }

    @Test
    public void fetchRoleByName() throws Exception{
        fetchRole("R_ADMIN")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",notNullValue()));
    }

    @Test
    @WithMockUser("admin")
    public void addUser() throws Exception {
        int numberOfUsers = getUsers().size();
        ResultActions response = newUser(randomString(5),getRole(randomString(5).toUpperCase()));
        response.andExpect(status().isOk())
        ;
        fetchUsers(null).
                andExpect(jsonPath("size()",equalTo(numberOfUsers+1)));
    }

    @Test
    @WithMockUser("admin")
    public void changePassword() throws Exception {
        AppUser user = getUser(null);
        RequestDTO req = new RequestDTO();
        req.setAppUser(user);
        mvc.perform(post("/api/v1/change-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.lastLoginAt",is(notNullValue())));
    }

    @Test
    @WithMockUser("admin")
    public void fetchUsers() throws Exception {
        ResultActions response = fetchUsers(null);
        response
                .andExpect(status().isOk())
                .andExpect(jsonPath("size()", greaterThan(0)));
    }

    @Test
    public void fetchFunction() throws Exception{
        RequestDTO req = new RequestDTO();
        int numberOfFunction = SERVICE_URL.getFunctions().size();
        mvc.perform(post("/api/v1/functions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("size()",is(numberOfFunction)));
    }

}
