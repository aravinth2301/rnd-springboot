package au.xbits.function;

import au.xbits.util._IntegrationTestBase;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestClientDBConfig extends _IntegrationTestBase {

    @Test
    @WithMockUser("admin")

    public void testClientDbConfig() throws Exception {
        ResultActions response = mvc.perform(get("/api/v1/test_client_db_conf"));

        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username",is("db1-user")));
    }
}
