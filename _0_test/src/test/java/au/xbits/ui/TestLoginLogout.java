package au.xbits.ui;

import org.junit.Test;
import org.openqa.selenium.By;

import au.xbits.util._UIBaseTest;

public class TestLoginLogout extends _UIBaseTest{


  @Test
  public void testLoginLogout() throws Exception {
	  
	loggedInAsAdmin();
	
    driver.get(baseUrl + "/app-login?logout");
    driver.findElement(By.name("username")).clear();
    driver.findElement(By.name("username")).sendKeys("admin");
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("admin");
    driver.findElement(By.cssSelector("input.btnSubmit")).click();
    driver.findElement(By.cssSelector("path")).click();
  }

 
}
