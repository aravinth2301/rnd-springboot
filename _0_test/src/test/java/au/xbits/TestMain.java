package au.xbits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import au.xbits.util.ApplicationConfig;

@SpringBootApplication
public class TestMain {
	public static void main(String[] args) {
		System.setProperty("spring.config.name", ApplicationConfig.appConfingName);
		SpringApplication.run(TestMain.class, args);
	}
}
